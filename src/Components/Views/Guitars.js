import { useState } from "react";

const Guitars = () => {
  //const guitars = [];
  const [guitars, setGuitars] = useState([]);

  const handleImageClick = ()=>{
    console.log("guitar selected")
}

  const models = guitars.map((guitar) => {
    return (
      <div key={guitar.id}>
        <h4>{guitar.model}</h4>
        <img src={guitar.image} alt={"Picture of" + guitar.model} onClick={handleImageClick}/>
      </div>
    );
  });

  const handleLoadGuitarData = async () => {
    const response = await fetch(
      "https://noroff-assignment-users-api.herokuapp.com/guitars"
    );
    const result = await response.json();
    setGuitars(result);
  };

  
  return (
    <div className="App">
      <button onClick={handleLoadGuitarData}>Load guitars</button>
      <h1>Guitar Store</h1>
      <div>{guitars.length > 0 ? models : <p>no guitars</p>}</div>
    </div>
  );
};

export default Guitars;
